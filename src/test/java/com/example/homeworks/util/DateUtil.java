package com.example.homeworks.util;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public interface DateUtil {

    static LocalDate getLocalDate(final Date date) {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

}
