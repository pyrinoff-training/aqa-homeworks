package com.example.homeworks.api.agify;

import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Cookie;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.is;

@Disabled
public class SimpleApiTest {

    @Test
    void restAssuredWayOne() {
        get("https://api.agify.io/?name=andrey")
                .then()
                .statusCode(200)
                .body("name", is("andrey"));
    }

    @Test
    void restAssuredWayTwo() {
        given()
                .when()
                .get("https://api.agify.io/?name=andrey")
                .then()
                .statusCode(200)
                .body("name", is("andrey"));
    }

    @Test
    void restAssuredWayWithLog() {
        given()
                .when()
                .log().all()
                .get("https://api.agify.io/?name=andrey")
                .then()
                .log().all()
                .statusCode(200)
                .body("name", is("andrey"));
    }


    @Test
    void restAssuredWayWithExtract() {
        String name = given()
                .when()
                .log().all()
                .get("https://api.agify.io/?name=andrey")
                .then()
                .log().all()
                .statusCode(200)
                .extract().path("name");
        Assertions.assertEquals("andrey", name);
    }

    @Test
    void restAssuredWayWithCookie() {
        //Получили по API токен (допустим)
        String authCookie = given()
                .when()
                .log().all()
                .get("https://api.agify.io/?name=andrey")
                .then()
                .log().all()
                .statusCode(200)
                .extract().cookie("x-request-id");
        //открыли браузер (т.к. иначе не подложить куку), причем на маленькой страничке или даже картинке
        open("/images/logo.png");
        //проставили куку
        getWebDriver().manage().addCookie(new Cookie("x-request-id", authCookie));
    }

    public static RequestSpecification requestSpecification = with()
            .log().uri()
            .log().headers()
            .log().body()
            //.filter(new AllureRestAssured())
            .contentType(ContentType.JSON)
            .baseUri("https://api.agify.io");


    @Test
    void restAssuredWayWithSpec() {
        given(requestSpecification)
                .when()
                .get("/?name=andrey")
                .then()
                .statusCode(200);
    }

}
