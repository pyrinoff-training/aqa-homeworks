package com.example.homeworks.api.gitlab.configuration;

import org.aeonbits.owner.Config;

@Config.Sources("classpath:application.properties")
public interface Settings extends Config {

    @Config.Key("gitlab.token")
    String gitlabPrivateToken();

}
