package com.example.homeworks.api.gitlab;

import com.example.homeworks.api.gitlab.configuration.Settings;
import org.aeonbits.owner.ConfigFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import static io.restassured.RestAssured.given;


public class DeleteProjectsTest {

    static Settings settings;

    @BeforeAll
    static void beforeAll() {
        settings = ConfigFactory.create(Settings.class, System.getProperties());
    }

    @ParameterizedTest(name = "Удаление проекта {0}")
    //@MethodSource
    @CsvFileSource(resources = "projects.txt")
    void removeProject(final String projectName) {
        System.out.println("Deleting " + projectName);
        System.out.println("Deleting " + URLEncoder.encode(projectName, StandardCharsets.UTF_8));
        System.out.println("Token: " + settings.gitlabPrivateToken());
        given()
                .header("Authorization", "Bearer " + settings.gitlabPrivateToken())
                .baseUri("https://gitlab.com/api/v4")
                .log().all()
                .urlEncodingEnabled(false)
                .delete("/projects/" + URLEncoder.encode(projectName, StandardCharsets.UTF_8))
                .then()
                .log().all()
                .statusCode(202);
    }

}
