package com.example.homeworks.ui.demoqa.abstraction;

import com.codeborne.selenide.Configuration;
import com.example.homeworks.ui.demoqa.page.AutomationPracticeFormPage;
import com.example.homeworks.util.AllureUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.Map;

public class TestBase {

    public final static AutomationPracticeFormPage registrationPage = new AutomationPracticeFormPage();

    @BeforeAll
    static void beforeAll() {
        AllureUtil.addSelenideLogListener();
        Configuration.baseUrl="https://demoqa.com";
        Configuration.browserSize="1920x1080";
        Configuration.pageLoadTimeout=60000;

        //Selenoid remote
        Configuration.browser = "chrome";
        Configuration.browserVersion = "100.0";
        Configuration.remote = "https://user1:1234@selenoid.autotests.cloud/wd/hub";

        //Video capabilities / selenoid
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("selenoid:options", Map.<String, Object>of(
                "enableVNC", true,
                "enableVideo", true
        ));
        Configuration.browserCapabilities = capabilities;

    }

    @AfterEach
    void tearDown() {
        AllureUtil.browserConsoleLogs();
        AllureUtil.screenshotAs("Last screenshot");
        AllureUtil.pageSource();
    }

}
