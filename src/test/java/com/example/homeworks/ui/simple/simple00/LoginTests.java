package com.example.homeworks.ui.simple.simple00;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

@Disabled
public class LoginTests {

    @Test
    void successfulLoginTest() {
        //Configuration.browserCapabilities = new ChromeOptions().addArguments("--remote-allow-origins=*");
        open("https://qa.guru/cms/system/login");

        $("[name=email]").setValue("qagurubot@gmail.com");
        $("[name=password]").setValue("qagurupassword");
        $(".btn-success").click();
        $(".main-header__login").click();

        $(".logined-form").shouldHave(text("QA_GURU_BOT"));
    }

}